package main

import "fmt"

type Container struct {
    Id    int
    Value int
    Sum   int
    Link  []*Container
}

func (c *Container) toMap(m map[int]*UpContainer, parent int) {
    if _, ok := m[c.Id]; !ok {
        m[c.Id] = &UpContainer{c.Id, c.Value, c.Sum, []int{}}
    }

    if parent != 0 {
        m[c.Id].Parent = append(m[c.Id].Parent, parent)
    }
    if p, ok := m[parent]; ok && p.Parent != nil {
        m[c.Id].Parent = append(m[c.Id].Parent, p.Parent...)
    }
    for _, cont := range c.Link {
        cont.toMap(m, c.Id)
    }
}

type UpContainer struct {
    Id     int
    Value  int
    Sum    int
    Parent []int
}

func (c *UpContainer) String() string {
    return fmt.Sprintf("Id:%d, Value:%d, Sum:%d\n", c.Id, c.Value, c.Sum)
}

var c8 = Container{8, 2, 0, []*Container{}}
var c7 = Container{7, 2, 0, []*Container{}}
var c6 = Container{6, 2, 0, []*Container{}}
var c5 = Container{5, 2, 0, []*Container{&c6}}
var c4 = Container{4, 4, 0, []*Container{&c8}}
var c3 = Container{3, 3, 0, []*Container{&c5}}
var c2 = Container{2, 3, 0, []*Container{&c5, &c7}}
var c1 = Container{1, 2, 2, []*Container{&c2, &c3, &c4}}

func main() {
    m := make(map[int]*UpContainer)
    c1.toMap(m, 0)
    sum(m)
    fmt.Printf("%v\n", m)
}

func sum(m map[int]*UpContainer) {
    for _, c := range m {
        c.Sum = c.Value
        visits := make(map[int]bool)
        for _, p := range c.Parent {
            if !visits[p] {
                visits[p] = true
                c.Sum += m[p].Value
            }
        }
    }
}

